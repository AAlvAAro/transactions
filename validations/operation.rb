module Validations
  module Operation
    def similar_transaction
      return unless @transaction.valid

      if @account.transactions.size >= 1
        previous_transaction = @account.transactions.last
        is_whitin_time_range = within_time_range?(
          Time.parse(@transaction.time),
          Time.parse(previous_transaction.time),
          120
        )

        if is_whitin_time_range && previous_transaction.merchant == @transaction.merchant &&
            previous_transaction.amount == @transaction.amount
          @data['violations'] << 'doubled-transaction'
          @transaction.valid = false
        end
      end
    end

    def transaction_recurrency
      return unless @transaction.valid

      if @account.transactions.size >= 2
        last_three = @account.transactions.last(2)
        second_prev_transaction = last_three.first

        is_whitin_time_range = within_time_range?(
          Time.parse(@transaction.time),
          Time.parse(second_prev_transaction.time),
          180
        )

        if is_whitin_time_range
          @data['violations'] << 'high-frequency-small-interval'
          @transaction.valid = false
        end
      end
    end

    def within_time_range?(t1, t2, seconds)
      (t1 - t2).abs <= seconds
    end
  end
end
