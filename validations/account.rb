module Validations
  module Account
    def card_exist?
      if @account.balance == 0
        @account.balance = @data['account']['availableLimit']
        @account.active = @data['account']['activeCard']
        @data['violations'] = []
      else
        @data['violations'] = ['account-already-initialized']
      end
    end

    def card_status
      unless @account.active
        @data['violations'] << 'card-not-active'
        @transaction.valid = false
      end
    end

    def available_balance
      return unless @transaction.valid

      if @transaction.amount > @account.balance
        @data['violations'] << 'insufficient-limit'
        @transaction.valid = false
      else
        @account.balance -= @transaction.amount
        @account.transactions << @transaction
      end
    end
  end
end
