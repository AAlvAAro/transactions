class CreateAccount
  include Validations::Account

  def initialize(account, data)
    @account = account
    @data = data
  end

  def perform
    card_exist?
  end

  def result
    ResultBuilder.build(@account, @data)
  end
end
