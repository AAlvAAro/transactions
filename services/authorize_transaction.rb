class AuthorizeTransaction
  include Validations::Account
  include Validations::Operation

  def initialize(account, data)
    @account = account
    @data = data
    @transaction = Transaction.new(data)
  end

  def perform
    card_status
    transaction_recurrency
    similar_transaction
    available_balance
  end

  def result
    ResultBuilder.build(@account, @data)
  end
end
