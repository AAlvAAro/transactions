class FileParser
  def self.load
    file = File.open(ARGV[0])
    file.map do |line|
      JSON.parse(line)
    end
  end
end
