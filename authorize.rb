require 'json'
require 'byebug'

require_relative 'models/account'
require_relative 'models/transaction'
require_relative 'factories/operation_factory'
require_relative 'validations/account'
require_relative 'validations/operation'
require_relative 'services/file_parser'
require_relative 'services/create_account'
require_relative 'services/authorize_transaction'
require_relative 'builders/result_builder'


module Authorize
  account = Account.new

  FileParser.load.each do |data|
    operation = OperationFactory.create(account, data)
    operation.perform
    puts operation.result
  end
end
