class ResultBuilder
  def self.build(account, data)
    {
      account: {
        activeCard: account.active,
        availableLimit: account.balance
      },
      violations: data['violations']
    }.to_json
  end
end
