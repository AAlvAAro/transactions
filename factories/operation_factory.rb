class OperationFactory
  def self.create(account, data)
    case data.keys.first
    when 'account'
      CreateAccount.new(account, data)
    when 'transaction'
      AuthorizeTransaction.new(account, data)
    end
  end
end
