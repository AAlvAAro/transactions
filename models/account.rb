class Account
  attr_accessor :balance, :active, :transactions

  def initialize
    @balance = 0
    @active = false
    @transactions = []
  end
end
