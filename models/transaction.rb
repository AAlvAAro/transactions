require 'time'

class Transaction
  attr_accessor :valid
  attr_reader :amount, :merchant, :time

  def initialize(data)
    transaction = data['transaction']

    @amount = transaction['amount']
    @merchant = transaction['merchant']
    @time = transaction['time']
    @valid = true
    data['violations'] = []
  end
end
