require 'spec_helper'

describe Account do
  it 'should initialize an account with default values' do
    expect(subject.balance).to eq(0)
    expect(subject.active).to eq(false)
    expect(subject.transactions).to be_empty
  end
end
