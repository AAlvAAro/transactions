require 'spec_helper'

describe Transaction do
 let(:data) do
    JSON.parse(
      {"transaction": {"merchant": "Burger King", "amount": 20, "time": "2019-02-13T10:00:00.000Z"}}.to_json
    )
  end
  let(:transaction) { Transaction.new(data) }

  it 'should initialize an account with default values' do
    expect(transaction.amount).to eq(data['transaction']['amount'])
    expect(transaction.merchant).to eq(data['transaction']['merchant'])
    expect(transaction.time).to eq(data['transaction']['time'])
    expect(transaction.valid).to be_truthy
  end
end
