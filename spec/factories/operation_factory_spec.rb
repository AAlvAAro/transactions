require 'spec_helper'

describe OperationFactory do
  let(:account) { Account.new }
  let(:account_data) do
     JSON.parse(
       {"account": {"activeCard": true, "availableLimit": 100}}.to_json
     )
  end
  let(:transaction_data) do
    JSON.parse(
      {"transaction": {"merchant": "Burger King", "amount": 20, "time": "2019-02-13T10:00:00.000Z"}}.to_json
    )
  end

  context 'when type is account' do
    it 'should return a CreateAccount service class' do
      expect(described_class.create(account, account_data)).to be_a(CreateAccount)
    end
  end

  context 'when type is transaction' do
    it 'should return an AuthorizeTransaction service class' do
      expect(described_class.create(account, transaction_data)).to be_a(AuthorizeTransaction)
    end
  end
end
