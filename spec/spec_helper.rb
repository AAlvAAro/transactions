require 'byebug'
require 'json'
require 'simplecov'

SimpleCov.start do
  add_filter 'spec'
  add_filter 'services/file_parser.rb'
end

require_relative '../models/account'
require_relative '../models/transaction'
require_relative '../factories/operation_factory'
require_relative '../validations/account'
require_relative '../validations/operation'
require_relative '../services/file_parser'
require_relative '../services/create_account'
require_relative '../services/authorize_transaction'
require_relative '../builders/result_builder'
