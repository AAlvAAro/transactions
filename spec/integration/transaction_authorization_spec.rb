require 'spec_helper'

describe 'TransactionAuthorization' do
  let(:account) { Account.new }
  let(:inactive_card) { File.open('spec/fixtures/inactive_card.txt').read }
  let(:amount_above_balance) { File.open('spec/fixtures/amount_above_balance.txt').read }
  let(:two_similar_transactions) { File.open('spec/fixtures/two_similar_transactions.txt').read }
  let(:three_subsequent_transactions) { File.open('spec/fixtures/three_subsequent_transactions.txt').read }
  let(:valid_transactions) { File.open('spec/fixtures/valid_transactions.txt').read }
  let(:many_valid_transactions) { File.open('spec/fixtures/many_valid_transactions.txt').read }

  describe 'invalid transactions' do
    context 'inactive card' do
      it 'should not affect the balance and return the right violation message' do
        operations = inactive_card.split("\n")

        operations.each.with_index do |line, index|
          data = JSON.parse(line)
          operation = OperationFactory.create(account, data)
          operation.perform
          result = JSON.parse(operation.result)

          if index == operations.size - 1
            expect(result['account']['activeCard']).to be_falsey
            expect(result['account']['availableLimit']).to eq(100)
            expect(result['violations'].first).to eq('card-not-active')
          end
        end
      end
    end

    context 'transaction amount is above the balance limit' do
      it 'should not affect the balance and return the right violation message' do
        operations = amount_above_balance.split("\n")

        operations.each.with_index do |line, index|
          data = JSON.parse(line)
          operation = OperationFactory.create(account, data)
          operation.perform
          result = JSON.parse(operation.result)

          if index == operations.size - 1
            expect(result['account']['availableLimit']).to eq(100)
            expect(result['violations'].first).to eq('insufficient-limit')
          end
        end
      end
    end

    context 'two similar transactions take place within 2 minutes or less' do
      it 'should not affect the balance and return the right violation message' do
        operations = two_similar_transactions.split("\n")

        operations.each.with_index do |line, index|
          data = JSON.parse(line)
          operation = OperationFactory.create(account, data)
          operation.perform
          result = JSON.parse(operation.result)

          if index == operations.size - 1
            expect(result['account']['availableLimit']).to eq(50)
            expect(result['violations'].first).to eq('doubled-transaction')
          end
        end
      end
    end

    context 'three subsequent transactions take place within 3 minutes or less' do
      it 'should not affect the balance and return the right violation message' do
        operations = three_subsequent_transactions.split("\n")

        operations.each.with_index do |line, index|
          data = JSON.parse(line)
          operation = OperationFactory.create(account, data)
          operation.perform
          result = JSON.parse(operation.result)

          if index == operations.size - 1
            expect(result['account']['availableLimit']).to eq(0)
            expect(result['violations'].first).to eq('high-frequency-small-interval')
          end
        end
      end
    end
  end

  describe 'valid transactions' do
    it 'should apply all transactions to the balance and return no violations' do
      operations = valid_transactions.split("\n")

      operations.each.with_index do |line, index|
        data = JSON.parse(line)
        operation = OperationFactory.create(account, data)
        operation.perform
        result = JSON.parse(operation.result)

        if index == operations.size - 1
          expect(result['account']['availableLimit']).to eq(0)
          expect(result['violations']).to be_empty
        end
      end
    end
  end

  describe 'many valid and ivalid transactions' do
    it 'should run each transaction and keep the right balance at the end' do
      operations = many_valid_transactions.split("\n")

      operations.each.with_index do |line, index|
        data = JSON.parse(line)
        operation = OperationFactory.create(account, data)
        operation.perform
        result = JSON.parse(operation.result)

        if index == operations.size - 1
          expect(result['account']['availableLimit']).to eq(120)
          expect(result['violations'].first).to eq('insufficient-limit')
        end
      end
    end
  end
end
