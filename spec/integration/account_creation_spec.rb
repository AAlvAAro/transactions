require 'spec_helper'

describe 'AccountCreation' do
  let(:account) { Account.new }
  let(:single_account_file) { File.open('spec/fixtures/single_account_creation.txt').read }
  let(:already_initialized_account_file) { File.open('spec/fixtures/already_initialized_account_creation.txt').read }

  context 'when an account creation operation is received' do
    it 'should return the balance and no violations' do
      single_account_file.split("\n").each do |line|
        data = JSON.parse(line)
        operation = OperationFactory.create(account, data)
        operation.perform
        result = JSON.parse(operation.result)
        expect(result['account']['availableLimit']).to eq(100)
        expect(result['violations']).to be_empty
      end
    end
  end

  context 'when an already initialized account creation operation is received' do
    it 'should return the original balance and its violation' do
      already_initialized_account_file.split("\n").each.with_index do |line, index|
        data = JSON.parse(line)
        operation = OperationFactory.create(account, data)
        operation.perform
        result = JSON.parse(operation.result)
        expect(result['account']['availableLimit']).to eq(100)

        # Only the last operation will fail and thus have a violation error
        if index == -1
          expect(result['violations'].first).to eq('already-initialized-account')
        end
      end
    end
  end
end
