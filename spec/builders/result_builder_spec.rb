require 'spec_helper'

describe ResultBuilder do
  let(:account) { Account.new }
  let(:account_data) do
    {"account": {"activeCard": true, "availableLimit": 100}}
  end
  let(:transaction_data) do
    {"transaction": {"merchant": "Burger King", "amount": 20, "time": "2019-02-13T10:00:00.000Z"}}
  end

  shared_examples 'an account resonse' do |obj|
    expect(obj['account']).to eq(account_data['account'])
    expect(obj['violations']).to be_empty
  end

  context 'when data comes fromn account creation' do
    it 'should return the same data and add violations array' do
      described_class.build(account, account_data) do |obj|
        it_behaves_like 'an account response', obj
      end
    end
  end

  context 'when data comes from a transaction' do
    it 'should build the response as the account data' do
      described_class.build(account, transaction_data) do |obj|
        it_behaves_like 'an account response', obj
      end
    end
  end
end
