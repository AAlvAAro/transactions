# Project Structure

The code has been organized in the following way

The `Authorize` module is the base of the application. It import all the dependencies and
runs the entry point of the program.

## FileParser
This is the entry point of the application, it takes the stdin and loads the file that matches it,
then calls to perform the operations and print out the results.

## OperationFactory
Is a factory that will provide the right service on which to run the transaction checks, depending
on the type of the operation: account or transaction

## Services
`CreateAccount` and `AuthorizeTransaction` are called to peform all the checks required for each type
of operation.

## Models
`Account` has the information of the account on which the operations will be performed. It will also
store the successful transactions

`Transaction` holds up the information obtained by the validation checks to see if the processing can
proceed or it should finish when a failed validation occurs

Models can be easily converted to map a database table in case we want to persist data.

## Validations
Are modules that wrap methods that will check the operation data against their specific rules. They
are organized in `Account` validations and `Operation` validations.

Future validations could be added to this namespace as new modules or new methods to the existing ones.

## ResultBuilder
It will be run after each operation to print out the result of it in the required format


# How to run the application

- Add as many files as you want to the root path. These files should contain
  a list of JSON data that represent the operation that will be performed by
  the application
- Build the docker container `docker build -t nubank_ruby .`
- You can send one file to the application in order to produce de results of
  the operations by running `docker run nubank_ruby ruby authorize.rb {file_name}`
- After this you'll be able to see the results of the operations printed out
- You might need to rebuild the container if additional files are aded to the
  project's folder
- There is a bunch of fixture files in the spec/fixtures folder, with can be used to run
  agains the program as well

# Testing

- A library to report test coverate has been added, the report is generated after the test suite
  is run. Project has currently 100% test coverage and the report can be found at coverage/
- All tests can be run with `docker run nubank_ruby rspec`
- Specific tests can be run by its namespace folder or individualy, like
  `docker run nubank_ruby rspec spec/integration` or
  `docker run nubank_ruby rspec spec/models/transaction_spec`
- A single test can be run by taking the line number on whith the test starts and add it at
  the command and a colon, lime
  `docker run nubank_ruby rspec spec/integration/transaction_authorization_spec.rb:50`

  that command will run the integration test for 2 transactions that produce a doubled-transaction

