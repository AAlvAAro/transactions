FROM ruby:2.5.7

RUN apt-get update && apt-get install

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
ADD spec/* $APP_HOME/
RUN bundle install

ADD . $APP_HOME
